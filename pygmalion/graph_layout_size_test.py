from __future__ import division
import igraph
import math



g = igraph.Graph.Barabasi(1000, 1)
width = 1000
density = math.ceil(width / g.vcount())
layout = g.layout('fr')
g.vs['size'] = [2 * math.ceil(math.log(x, 2)) + density for x in g.degree()]
igraph.plot(g, 'resulting_graph.png', vertex_color='#00000066',
            edge_color='#22222233', edge_width=1.5, layout=layout, bbox=(0, 0, width, width))
