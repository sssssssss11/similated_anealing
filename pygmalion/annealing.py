# -*- coding: utf-8 -*-
from __future__ import division
from collections import defaultdict, Counter
import itertools
import logging
import logging.config
import math
import random

from numpy import nan
import yaml

from utilities import Animation
from pygmalion import Pygmalion
from weightcalculation.analisys import get_diff


logging.config.dictConfig(yaml.load(open('logging.conf', 'r')))
logger = logging.getLogger('annealing')


class TemperatureKeeper(object):
    def __init__(self, high_temperature, low_temperature, cooling_rate, keep_temperature=1):
        self.init_temperature = high_temperature
        self.temperature = high_temperature
        self.low_temperature = low_temperature
        self.cooling_rate = cooling_rate
        self.keep_temp = keep_temperature
        self.cool_steps = int(keep_temperature * math.log(low_temperature / high_temperature, cooling_rate))
        self.t = 0

    def cool_down(self):
        # self.temperature *= self.cooling_rate
        self.temperature = self.init_temperature / (1 + self.cooling_rate * self.t)
        self.t += 1

    def is_hot(self):
        return self.temperature > self.low_temperature


class SimulatedAnnealing(object):
    def __init__(self, termometer, system, log_on=True):
        self.termometer = termometer
        self.system = system
        self.current_iteration = 0
        self.log_keeper = defaultdict(lambda: list())
        self.log_keeper['fitness'] = defaultdict(lambda: list())
        self.current_energy = self.system.get_system_energy()
        self.log_on = log_on
        # initialization
        self.prop_log = []
        self.add_del_dict = {}
        self.arr_add = []
        self.arr_del = []
        self.edge_arr = []
        self.energy_arr = []
        self.clust_arr = []
        self.density_arr = []
        self.diff_between_assets = {}
        self.diff_between_liabilities = {}
        # self.animation = Animation(self)

    def run_annealing(self, logging=True):
        # while self.termometer.is_hot() and self.current_energy > 0.005:

        while self.termometer.is_hot():
            for i in range(self.termometer.keep_temp):
                self.current_iteration += 1
                self.make_step()
            self.termometer.cool_down()
        # Forming dict of added and deleted edges
        self.add_del_dict['add'] = self.arr_add
        self.add_del_dict['del'] = self.arr_del
        return self.system.best_proposal

    def make_step(self):
        proposal_g, proposed_energy = self.system.make_proposal(self.log_keeper)
        # forming array of tuplets where first element added/deleted edge, second iteration
        self.edge_arr.append((self.system.edge_count, self.current_iteration))
        self.arr_add.append((self.system.addE, self.current_iteration))
        self.arr_del.append((self.system.delE, self.current_iteration))
        self.energy_arr.append((self.current_energy, self.current_iteration))
        #
        self.log_keeper['proposed_energy'].append(proposed_energy)
        if self.accept_proposal(proposed_energy):
            # added array to save clust and density changing
            self.clust_arr.append((self.system.fitness.print_values(proposal_g)['Clusterization'],self.current_iteration))
            self.density_arr.append((self.system.fitness.print_values(proposal_g)['Density'], self.current_iteration))
            #
            self.system.save_proposal((proposal_g, proposed_energy), iter=self.current_iteration)
            self.current_energy = proposed_energy
            self.log_keeper['accepted'].append(1)
        else:
            self.log_keeper['accepted'].append(0)
        self.log_keeper['edge_count'].append(self.system.g.ecount())

        self.log_keeper['energy'].append(self.current_energy)
        self.diff_between_assets[self.current_iteration] = get_diff(self.system.g, "assets")
        self.diff_between_liabilities[self.current_iteration] = get_diff(self.system.g, "liabilities")
        if self.log_on:
            if self.current_iteration % 50 == 0 & logger.isEnabledFor(logging.DEBUG):
                acc = self.log_keeper['accepted'][-50:]

                d_acc = len(filter(lambda x: x == 1, acc)) / 50.0
                d_rej = len(filter(lambda x: x == 0, acc)) / 50.0
                logger.info("%d/%d\t%f\t%f\tacc:%f rej:%f" % (self.current_iteration, self.termometer.cool_steps,
                                                              self.current_energy, self.termometer.temperature,
                                                              d_acc, d_rej))

    def accept_proposal(self, proposal_e):
        delta = abs(self.current_energy - proposal_e)

        self.log_keeper['delta'].append(delta)

        if proposal_e <= self.current_energy:
            self.log_keeper['decisions'].append(nan)
            return 1

        prob = math.exp(-0.01 / self.termometer.temperature)
        # prob = math.exp((self.current_energy - proposal_e) / self.termometer.temperature)
        self.log_keeper['decisions'].append(prob)
        return False
        # return random.random() < 0.02
        # return random.random() < prob


class NetworkModifier(object):
    FIX_EDGE = 0
    REMOVE_EDGE = 1
    REWIRE_EDGE = 2

    def __init__(self, g, fitness, edge_fixes=50, edge_removals=5, edge_rewirings=10, mixage=0.5):
        self.g = g
        # last changes
        # self.g.es["weight"] = range(g.ecount())
        self.g['generation'] = 0
        self.fitness = fitness
        self.pygmalion = Pygmalion(g)
        self.best_proposal = (None, -1)
        self.all_best_proposals = []
        self.edge_fixes = edge_fixes
        self.edge_removals = edge_removals
        self.edge_rewirings = edge_rewirings
        self.previous_fit = None
        self.corr = defaultdict(lambda: 0)
        self.fit_hist = defaultdict(lambda: [])
        self.mixage = mixage
        self.only_rewire = edge_fixes == edge_fixes == 0
        self.iter = -1
        self.addE = 0
        self.delE = 0
        self.edge_count = 0

    def get_system_energy(self):
        return self.fitness.calculate_fitness(self.g)[0]

    def process_correlations(self, proposal_fit):
        if self.previous_fit:
            res = {}
            for k in proposal_fit:
                res[k] = (-1) ** (1 + (proposal_fit[k] - self.previous_fit[k] >= 0))
                self.fit_hist[k].append(proposal_fit[k] - self.previous_fit[k])
            for a, b in itertools.combinations(proposal_fit.keys(), 2):
                self.corr['_'.join(sorted([a, b]))] += res[a] == res[b]
        self.previous_fit = proposal_fit

    def make_proposal(self, log_keeper):
        proposal_g = self.g.copy()
        self.edge_count = len(self.g.get_edgelist())
        random_value = random.random()
        if random_value <= 1 / 3 and not self.only_rewire:
            log_keeper['action'].append(self.FIX_EDGE)
            if random.random() <= self.mixage:
                self.pygmalion.add_edge_simple(proposal_g, times=self.edge_fixes, shift=False)
            else:
                self.pygmalion.add_edge_preferential(proposal_g, times=self.edge_fixes, shift=False)
        elif 1 / 3 < random_value <= 2 / 3 and not self.only_rewire:
            log_keeper['action'].append(self.REMOVE_EDGE)
            self.pygmalion.remove_random_edge(proposal_g, times=self.edge_removals, shift=False)
        else:
            log_keeper['action'].append(self.REWIRE_EDGE)
            if random.random() <= self.mixage:
                self.pygmalion.rewire_random_edges(proposal_g, times=self.edge_rewirings, shift=False)
            else:
                self.pygmalion.rewire_preferential_edges(proposal_g, times=self.edge_rewirings, shift=False)
                #add arr of added and deleted edges
                self.addE = self.pygmalion.add_del_dict['add']
                self.delE = self.pygmalion.add_del_dict['del']

        proposal_energy, proposal_fitnesses, proposal_values = self.fitness.calculate_fitness(proposal_g)
        for k, v in proposal_fitnesses.items():
            log_keeper['fitness'][k].append(v)
        self.process_correlations(proposal_fitnesses)
        return proposal_g, proposal_energy

    def save_proposal(self, proposal, iter):
        self.g = proposal[0]
        self.g['generation'] += 1

        if self.best_proposal[1] == -1 or proposal[1] < self.best_proposal[1]:
            self.best_proposal = proposal
            self.all_best_proposals.append(proposal[0])
            self.iter = iter
