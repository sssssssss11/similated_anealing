# -*- coding: utf-8 -*-
from __future__ import division
from collections import Counter
from collections import defaultdict
import random
import math

import igraph
import numpy as np


def zeta(s):
    summary = 0
    for i in range(1, 100):
        summary += 1 / (i ** s)
    return s


zet = {}
for x in range(1, 300):
    zet[x] = zeta(x)


def power_law(k, a):
    return pow(k, -a) / zeta(a)


def get_probabilities(n=100, alpha=2):
    probabilities = [0]
    for k in range(1, 200 + 1):
        probabilities.append(power_law(k, alpha))
    return probabilities


def get_net_probs(g):
    c = Counter(g.degree())
    probs = []
    max_key = max(c.keys())
    for k in range(0, max_key + 1):
        probs.append(c[k] / g.vcount())
    return probs


def calc_integers(current, desired, maximum=1, smoothing=0.1):
    """Calculate difference between values value >= 1.0"""
    return 1 / (smoothing * abs(current - desired) / maximum + 1)


def calc_floats(current, desired):
    """Calculate difference between values 0.0 <= value <= 1.0"""
    return 1 / (abs(current - desired) + 1)


def calc(current, desired, max_val):
    """Calculate difference between values 0.0 <= value <= 1.0"""
    return 1 - abs(current - desired) / max_val


class Fitness:
    def __init__(self):
        self.fitness_set = []
        self.best_fitnesses = defaultdict(lambda: 2)
        self.p = 3

    def add(self, func_descriptor):
        if func_descriptor:
            self.fitness_set.append(func_descriptor)

    def calculate_fitness(self, g):
        if len(self.fitness_set) == 0:
            raise Exception('Fitness set is empty or graph is not present!')
        fitnesses = {}
        values = {}
        fitness = 0.0
        for f, w in self.fitness_set:
            f_fitness, f_value = f.calculate_fitness(g)
            fitness_norm = 2 - f_fitness
            fitness_fin = (fitness_norm / self.best_fitnesses[str(f)]) ** self.p
            fitness += fitness_fin
            fitnesses[str(f)] = fitness_norm
            values[str(f)] = f_value
            if self.best_fitnesses[str(f)] > fitness_norm:
                    self.best_fitnesses[str(f)] = fitness_norm

        #fitness **= 1 / self.p
        return fitness, fitnesses, values

    def calculate_fitness3(self, g):
        if len(self.fitness_set) == 0:
            raise Exception('Fitness set is empty or graph is not present!')
        fitnesses = {}
        values = {}
        fitness = 0.0
        for f, w in self.fitness_set:
            f_fitness, f_value = f.calculate_fitness(g)
            if self.best_fitnesses[str(f)] == 0.0000001:
                fitness_fin = 0
            else:
                fitness_fin = (f_fitness / self.best_fitnesses[str(f)]) ** self.p
            fitness += fitness_fin
            fitnesses[str(f)] = f_fitness
            values[str(f)] = f_value
            if self.best_fitnesses[str(f)] < f_fitness:
                self.best_fitnesses[str(f)] = f_fitness

        #fitness **= self.p
        fitness *= -1
        return fitness, fitnesses, values

    def print_fitness(self, g):
        if len(self.fitness_set) == 0:
            raise Exception('Fitness set is empty or graph is not present!')
        fit = {}
        for f, w in self.fitness_set:
            value = f.get_optimized_value(g)
            if type(value) == int:
                fit[str(f)] = str(value) + '/' + str(f.v)
            else:
                fit[str(f)] = '{0:0.4f}'.format(value) + '/' + '{0:0.4f}'.format(f.v)
        return fit


class FitnessCalculator(object):
    name = 'Noname function'

    def calculate_fitness(self, g):
        pass  # another

    def get_optimized_value(self, g):
        pass

    def __str__(self):
        return self.name


class DistributionFitnessCalculator(FitnessCalculator):
    def __init__(self, alpha):
        self.alpha = alpha

    def get_optimized_value(self, g):
        deg = np.array(g.degree()) + 1
        return igraph.power_law_fit(deg, method='auto', return_alpha_only=True)

    def calculate_fitness(self, g):
        deg = np.array(g.degree()) + 1
        al = igraph.power_law_fit(deg, method='auto', return_alpha_only=True)
        fitness = 1.0 - (abs(self.alpha - al) / max(self.alpha, al))
        return fitness


class PowerlawFitnessCalculator(FitnessCalculator):
    def __init__(self, alpha):
        self.alpha = alpha

    def calculate_fitness(self, g):
        s2 = get_net_probs(g)
        high_cut = -1
        for i, d in enumerate(reversed(s2)):
            if d > 0:
                high_cut = i
                break

        if high_cut > 0:
            s2 = s2[0:-high_cut]

        s1 = get_probabilities(g.vcount(), self.alpha)
        s1 = s1[0:len(s2)]

        l = 0
        for x, y in zip(s1, s2):
            l += (x - y) ** 2
        l /= len(s2)
        l = abs(1 - math.sqrt(l))

        return l


class ClusterisationFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Clusterization'

    def get_optimized_value(self, g):
        return g.transitivity_avglocal_undirected(mode='zero')

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc(value, self.v, 1.0), value


class DensityFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Density'
        self.max_value = 1.0

    def get_optimized_value(self, g):
        return 2 * g.ecount() / (g.vcount() * (g.vcount() - 1))

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc(value, self.v, self.max_value), value


class DanglingFitnessCalculator(FitnessCalculator):
    def __init__(self):
        self.name = ''
        pass

    def get_optimized_value(self, g):
        return len([x for x in g.degree() if x == 0])

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc_integers(value, 0), value


class ComponentsFitnessCalculator(FitnessCalculator):
    def __init__(self):
        self.name = 'Components'
        self.v = 1
        pass

    def get_optimized_value(self, g):
        return len(g.components())

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        #return 1 / value, value
        return calc_integers(value, 1), value
        #return calc(value, 1, g.vcount()), value


class DiameterFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Diameter'

    def get_optimized_value(self, g):
        return g.diameter()

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc(value, self.v, g.vcount()), value


class CommunityFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Community'

    def get_optimized_value(self, g):
        # return g.community_walktrap().optimal_count
        return g.community_fastgreedy().optimal_count
        # return g.community_edge_betweenness(directed=False).optimal_count

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc_integers(value, self.v), value


class ModularityFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Modularity'

    def get_optimized_value(self, g):
        return g.community_fastgreedy().as_clustering().modularity
        # return g.community_fastgreedy().optimal_count
        # return g.community_edge_betweenness(directed=False).optimal_count

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        #return calc(value, self.v, 1.0), value
        return calc_floats(value, self.v), value


class AssortativityFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        if abs(v) > 1:
            raise Exception('Wrong assortativity value')

        self.v = v
        self.name = 'Assortativity'

    def get_optimized_value(self, g):
        return g.assortativity_degree(directed=False)

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)

        if str(value) == 'nan':
            return 0, value

        return calc_floats(value, self.v), value


class EfficiencyCalculator():
    def __init__(self):
        self.name = 'Efficiency'
        self.v = 1
        # self.init_value = self.calculate_efficiency(g)

    @staticmethod
    def calculate_efficiency(g):
        """Efficiency calculation"""
        result = 0.0
        paths = g.shortest_paths()
        if g.is_directed():
            for path in paths:
                for distance in path:
                    if 0 < distance < float('Inf'):
                        result += float(1) / distance
        else:
            for index, path in enumerate(paths):
                for distance in path[index + 1:]:
                    if 0 < distance < float('Inf'):
                        result += float(2) / distance

        n = g.vcount()
        result /= n * (n - 1)
        return result

    def get_optimized_value(self, g):
        return self.calculate_efficiency(g)

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc_floats(value, 1.0), value


class DegreeSequenceFitCalculator(FitnessCalculator):
    def __init__(self, sequence):
        self.sequence = sequence
        self.c_desired = Counter(self.sequence)
        self.sequence.sort()
        self.maximum = 1
        self.name = 'Degree sequence'
        self.v = 0.0
        # self.init_value = self.calculate_efficiency(g)

    # def calculate_sequence_fit(self, g):
    #     if g.vcount() != len(self.sequence):
    #         raise Exception('Sequence length is not equal nodes count')
    #     present_sequence = g.degree()
    #     present_sequence.sort()
    #     return sum(map(operator.abs, map(operator.sub, present_sequence, self.sequence)))

    def calculate_sequence_fit(self, g):
        if g.vcount() != len(self.sequence):
            raise Exception('Sequence length is not equal nodes count')
        present_sequence = g.degree()
        c_degrees = Counter(present_sequence)
        c_degrees.subtract(self.c_desired)
        return sum([abs(x) for x in c_degrees.values()]) / (g.vcount() * 2.0)

    def get_optimized_value(self, g):
        return self.calculate_sequence_fit(g)

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        if self.maximum < abs(value - 0):
            self.maximum = abs(value - 0)

        # div = max(curr_m, self.init_value)
        # fitness = abs(1 - abs(curr_m - self.m) / div)
        return calc(value, 0, 1.0), value
        #return calc_floats(value, 0), value


class MeanDegreeFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Mean Degree'

    def get_optimized_value(self, g):
        return np.array(g.degree()).mean()

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc(value, self.v, g.vcount() - 1), value


class SirFitnessCalculator(FitnessCalculator):
    def __init__(self, v, alpha=0.3, beta=0.5, runs=10):
        self.alpha = alpha
        self.beta = beta
        self.runs = runs
        self.v = v
        self.name = 'Sir percentage of R'

    def make_sir(self, g_or, nei=None):
        g = g_or.copy()
        pop_size = g.vcount()
        iList = set()
        rList = set()
        sList = set(range(pop_size))
        f = random.randint(0, pop_size - 1)
        # f = 1

        iList.add(f)
        sList.remove(f)

        if not nei:
            for k in range(pop_size):
                nei[k] = set(g.neighbors(g.vs[k]))

        while len(iList) > 0:

            newI = set()
            newR = set()
            tempS = set()

            for i in iList:
                tempS |= nei[i]
            tempS -= rList
            tempS -= iList
            for s in tempS:
                if random.random() < self.alpha:
                    newI.add(s)
            for i in iList:
                if random.random() < self.beta:
                    newR.add(i)

            sList -= newI
            iList -= newR
            iList |= newI
            rList |= newR
        return float(len(rList)) / pop_size

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc_floats(value, self.v), value

    def get_optimized_value(self, g):
        nei = dict()
        neig = g.neighbors

        v = g.vs
        for k in range(g.vcount()):
            nei[k] = set(neig(v[k]))

        result = []
        for n in range(self.runs):
            l = self.make_sir(g, nei=nei)
            result.append(l)

        return np.array(result).mean()


class SirDaysFitnessCalculator(FitnessCalculator):
    def __init__(self, v, alpha=0.3, beta=0.5, runs=10):
        self.alpha = alpha
        self.beta = beta
        self.runs = runs
        self.v = v
        self.name = 'Sir days'

    def make_sir(self, g_or, nei=None):
        g = g_or.copy()
        pop_size = g.vcount()
        iList = set()
        rList = set()
        sList = set(range(pop_size))
        # f = random.randint(0, pop_size - 1)
        f = 1

        iList.add(f)
        sList.remove(f)
        iterations = 0

        if not nei:
            for k in range(pop_size):
                nei[k] = set(g.neighbors(g.vs[k]))

        while len(iList) > 0:
            iterations += 1
            new_i = set()
            new_r = set()
            temp_s = set()

            for i in iList:
                temp_s |= nei[i]
            temp_s -= rList
            temp_s -= iList
            for s in temp_s:
                if random.random() < self.alpha:
                    new_i.add(s)
            for i in iList:
                if random.random() < self.beta:
                    new_r.add(i)

            sList -= new_i
            iList -= new_r
            iList |= new_i
            rList |= new_r
        return iterations

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        if abs(value - self.v) < 2:
            return 1
        else:
            return calc_integers(value, self.v)

    def get_optimized_value(self, g):
        nei = dict()
        neig = g.neighbors

        v = g.vs
        for k in range(g.vcount()):
            nei[k] = set(neig(v[k]))

        result = []
        for n in range(self.runs):
            l = self.make_sir(g, nei=nei)
            result.append(l)

        return np.array(result).mean()