import itertools
import random


class Pygmalion(object):
    def __init__(self, g):
        self._edge_pairs = list(itertools.combinations(list(range(0, g.vcount())), 2))

    def fix_edge(self, g, times=1, shift=False):
        if shift:
            times = self.get_random(times)

        g.add_edges(random.sample(self._edge_pairs, times))
        g.simplify()

    def remove_random_edge(self, g, times=1, shift=False):
        if shift:
            times = self.get_random(times)

        if g.ecount() < times:
            return

        selection = random.sample(range(g.ecount()), k=times)
        g.delete_edges(selection)

    def rewire_random_edges(self, g, times=1, shift=False):
        if g.ecount() <= 1:
            return

        if shift:
            times = self.get_random(times)

        if g.ecount() < 1:
            return

        g.rewire(times)

    def rewire_random_edge(self, g, times=1, shift=False):
        if g.ecount() <= 1:
            return
        selection = random.sample(range(g.ecount()), k=times)
        pairs = [(g.es[x].source, g.es[x].target) for x in selection]

        to_add = []
        for p in pairs:
            s = random.choice(p)
            t = random.randint(0, g.vcount() - 1)
            to_add.append((s, t))


        g.delete_edges(selection)
        g.add_edges(to_add)
        g.simplify()

    @staticmethod
    def get_random(times):
        return random.randint(times // 2, times * 2)

    @staticmethod
    def not_edge(g, edges):
        g.add_edges(edges)
        g.simplify()
