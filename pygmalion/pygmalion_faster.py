import itertools
import random


class Pygmalion(object):
    def __init__(self, g):
        self._nonpresent_edge_pairs = set(itertools.combinations(list(range(0, g.vcount())), 2))
        self._untouched_nonpresent_edge_pairs = set(itertools.combinations(list(range(0, g.vcount())), 2))
        self._present_edge_pairs = set()

    @staticmethod
    def get_random(times):
        return random.randint(times // 2, times * 2)

    def fix_edge(self, g, times=1, shift=False):
        if shift:
            times = self.get_random(times)

        add_edges_set = random.sample(self._untouched_nonpresent_edge_pairs.difference(set(g.get_edgelist())), times)

        if times == 1:
            g.add_edge(add_edges_set[0])
        else:
            g.add_edges(add_edges_set)

    def remove_random_edge(self, g, times=1, shift=False):
        if shift:
            times = self.get_random(times)

        if g.ecount() < 5 * times:
            return

        selection = random.sample(g.get_edgelist(), times)
        g.delete_edges(selection)

    def rewire_random_edges(self, g, times=1, shift=False):
        if g.ecount() <= 1:
            return

        if shift:
            times = self.get_random(times)

        if g.ecount() < 1:
            return

        g.rewire(times)

    def not_edge(self, g, edges):
        #to_add = []
        #for node_a, node_b in edges:
        #    to_add.append((node_a, node_b))
        #g.add_edges(to_add)

        g.add_edges(edges)
        g.simplify()

