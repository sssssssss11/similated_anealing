import itertools
import random
from weightcalculation.weightcalc import add_edge_with_weight, delete_edge_with_weight, delete_edge_after_add

# wtf???
def p(a, b):
    if a < b:
        return a, b
    else:
        return b, a


# The commented code is the source code
class Pygmalion(object):
    def __init__(self, g):
        self.vertices = g.vcount()
        # create all edges in graph
        self._edge_pairs = list(itertools.combinations(list(range(0, g.vcount())), 2))
        # my added
        self.add_del_dict = {}
        self._assets = dict(zip(range(0, g.vcount()), g.vs['assets']))
        self._liabilities = dict(zip(range(0, g.vcount()), g.vs['liabilities']))
        self._edges_dict = dict(zip(set([e.tuple for e in g.es]), g.es['weight']))
        #self._assets = list(itertools.combinations(g.vs['assets'], 2))
        #self._liabilities = list(itertools.combinations(g.vs['liabilities'], 2))
        #self._assets_dict = dict(zip(self._edge_pairs, self._assets))
        #self._liabilities_dict = dict(zip(self._edge_pairs, self._liabilities))

    def _v2(self, index, g):
        a = 0.0
        l = 0.0
        for i in range(0, g.vcount()):
            if (index, i) in self._edges_dict:
                if self._edges_dict[(index, i)] is not None:
                    l += self._edges_dict[(index, i)]
            if (i, index) in self._edges_dict:
                if self._edges_dict[(i, index)] is not None:
                    a += self._edges_dict[(i, index)]

        AD = abs(a - self._assets[index])
        LD = abs(l - self._liabilities[index])
        # return abs(AD*AD - LD*LD)
        return min(AD,LD)

    def add_edge_simple(self, g, times=1, shift=False):
        if shift:
            times = self.get_random(times)

        # random added edges
        for i in random.sample(self._edge_pairs, times):
            add_edge_with_weight(g, i)
            # g.add_edge(i[0], i[1], weight=self._v2(i[0], g))

        self._edges_dict = dict(zip(set([e.tuple for e in g.es]), g.es['weight']))
        # source code
        # g.add_edges(random.sample(self._edge_pairs, times))
        # g.simplify()

    def add_edge_preferential(self, g, times=1, shift=False):
        if shift:
            times = self.get_random(times)
        vertex_set = random.sample(range(g.vcount()), times)
        # new_edges = set()

        sample = [2, 2, 2, 3, 3, 4] # pick how far we look for new friend: fof, fofof, fofofof
        front = random.choice(sample)
        n_far = g.neighborhood(vertex_set, order=front)
        n_close = g.neighborhood(vertex_set, order=front - 1)
        for i, v in enumerate(vertex_set):
            potential_n = list(set(n_far[i]) - set(n_close[i]))
            if potential_n:
                new_neigbor = random.choice(potential_n)
                # g.add_edge(v, new_neigbor, weight=self._v2(v, g))
                add_edge_with_weight(g, (v, new_neigbor))
                # new_edges.add(p(v, new_neigbor))

        self._edges_dict = dict(zip(set([e.tuple for e in g.es]), g.es['weight']))
        #   g.add_edges(new_edges)

    def remove_random_edge(self, g, times=1, shift=False):
        if shift:
            times = self.get_random(times)

        if g.ecount() < times:
            return
        # delete random edges
        selection = random.sample(range(g.ecount()), k=times)
        # edges_list = g.get_edge_list()
        # g.delete_edges(selection)
        delete_edge_with_weight(g, selection)
        self._edges_dict = dict(zip(set([e.tuple for e in g.es]), g.es['weight']))

    def rewire_random_edges(self, g, times=1, shift=False):
        if g.ecount() <= 1:
            return

        if shift:
            times = self.get_random(times)

        if g.ecount() < 1:
            return

        # Overwriting edges in graph times times
        g.rewire(times)

    def rewire_preferential_edges(self, g, times=1, shift=False):
        if g.ecount() < times:
            return
        selection = random.sample(range(g.ecount()), k=times)
        pairs = [(g.es[x].source, g.es[x].target) for x in selection]
        to_add = set()
        to_remove = set()

        for i, j in pairs:
            nei = g.neighborhood([i, j], order=1)

            i_sugg = list(set(nei[0]) - set(nei[1]))
            j_sugg = list(set(nei[1]) - set(nei[0]))
            if i_sugg and j_sugg:
                i_suggestion = random.choice(i_sugg)
                j_suggestion = random.choice(j_sugg)
                # to_add.append((i_suggestion, j))
                # to_add.append((j_suggestion, i))
                # to_remove.append((j_suggestion, j))
                # to_remove.append((i_suggestion, i))
                to_add.add(p(i_suggestion, j))
                to_add.add(p(j_suggestion, i))
                to_remove.add(p(j_suggestion, j))
                to_remove.add(p(i_suggestion, i))

        l = min(len(to_add), len(to_remove))
        add_e = 0
        del_e = 0
        for i in list(to_add):
            add_e += 1
            # last changes
            # g.add_edge(i[0], i[1], weight=self._v2(i[0], g))
            add_edge_with_weight(g, i)
        for i in list(to_remove):
            del_e += 1
            #g.delete_edges(i[0], i[1])
            delete_edge_after_add(g, i)
        self.add_del_dict['add'] = add_e
        self.add_del_dict['del'] = del_e
        self._edges_dict = dict(zip(set([e.tuple for e in g.es]), g.es['weight']))
        # =g.simplify()

    @staticmethod
    def get_random(times):
        return random.randint(times // 2, times * 2)

    @staticmethod
    def not_edge(g, edges):
        g.add_edges(edges)
        g.simplify()

if __name__ == '__main__':
    a = list(itertools.combinations([1, 2, 3], 2))
    b = list(itertools.combinations(['a', 'b', 'c'], 2))
    test = dict(zip(a, b))
    print test[(1, 2)]