def sum_of_neighboor(graph, vertex, mode):
    """
    Cumpute summury value of assets of all vertecies which neighbor to vertex
    :param graph: igraph.Graph
    :param vertex: vertex id
    :param mode: "in" - for assets "out" - for lianilities
    :return: sum of vertex neigbors
    """
    arr = graph.neighborhood(vertex, mode=mode)[1:]
    if arr:
        return sum(map(lambda x: graph.vs["assets"][x], arr))
    else:
        return 0
