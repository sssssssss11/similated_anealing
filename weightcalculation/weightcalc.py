import igraph


def sum_of_neighboor(graph, vertex):
    """
    Cumpute summury value of assets of all vertecies which neighbor to vertex
    :param graph:
    :param vertex: index of vertex
    :return:
    """
    arr = graph.neighborhood(vertex, mode="out")[1:]
    if arr:
        return reduce(lambda x, y: x+y,
                      map(lambda x: graph.vs["assets"][x], arr))
    else:
        return 0


def add_weight(graph, edge):
    """
    Compute edge's weight
    :param graph:
    :param edge: graph edge, represents as a tuple
    :return:
    """
    out, to = edge
    assets = graph.vs["assets"][to]
    liabilities = graph.vs["liabilities"][out]
    summ = sum_of_neighboor(graph, out)
    if summ != 0:
        graph[out, to] = liabilities/summ*assets
    else:
        # week place, what if liabilities > assets
        graph[out, to] = liabilities


def update_weight(graph, vertex):
    """
    Update all edge's weight which are neighbor of vertex
    :param graph: graph to mutate
    :param vertex: index vertex
    :return:
    """
    for i in graph.neighborhood(vertex, mode="out")[1:]:
        edge = (vertex, i)
        add_weight(graph, edge)


def add_edge_with_weight(graph, edge):
    """
    :param graph: graph to mutate
    :param edge: tuple of vertex index
    :return:
    """
    start, end = edge
    graph.add_edge(start, end, weight=0)
    update_weight(graph, start)


def delete_edge_with_weight(graph, sample):
    graph.delete_edges(sample)
    e_lst = graph.get_edgelist()
    for i in sample:
        try:
            start, end = e_lst[i]
            update_weight(graph, start)
        except IndexError:
            pass


def delete_edge_after_add(graph, edge):
    e_lst = graph.get_edgelist()
    graph.delete_edges(edge)
    if edge in e_lst:
        start, end = edge
        update_weight(graph, start)


if __name__ == '__main__':
    g = igraph.Graph.Read_GraphML("test_10.graphml")
    print g
    old = g.es["weight"]
    g[0,1] = 322
    new_one = g.es["weight"]
