from igraph import *
import random


def create_properties(graph, weight_dict, mode=""):
    lst = [0 for x in range(graph.vcount())]
    for i in range(graph.vcount()):
        for j in graph.neighborhood(i, mode=mode)[1:]:
            if mode == "in":
                lst[i] += weight_dict[(j, i)]
            if mode == "out":
                lst[i] += weight_dict[(i, j)]

    return lst


def give_random_weight(graph):
    graph.es["weight"] = 0
    for i in graph.get_edgelist():
        graph[i[0], i[1]] = random.randint(10, 100)
    d = dict(zip(graph.get_edgelist(), graph.es['weight']))
    return graph, d


def create_random_graph(n):
    g = Graph.Erdos_Renyi(n, 0.33, directed=True)
    g, d = give_random_weight(g)
    g.vs["assets"] = create_properties(g, d, mode="in")
    g.vs["liabilities"] = create_properties(g, d, mode="out")
    return g


def write_to_gml(graph):
    s = "test_{0}.graphml".format(graph.vcount())
    graph.write_graphml(s)
    print "writing as {0}".format(s)

if __name__ == '__main__':
    g = create_random_graph(10)
    print g.vs["liabilities"]
    print g.vs["assets"]
    g.es["weight"]
    write_to_gml(g)
