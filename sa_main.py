# -*- coding: utf-8 -*-
import datetime
import os
import igraph
import shutil
import matplotlib.pyplot as plt
from pygmalion.annealing import *
from pygmalion.fitness_with_scale import *
from pygmalion import utilities
from test import *

fitness = Fitness()
# fitness.add((AssortativityFitnessCalculator(-0.2), 0.1))
fitness.add((ClusterisationFitnessCalculator(0.087), 0.1))
fitness.add((DensityFitnessCalculator(0.062), 3))
#fitness.add((components_fit, 1))
# fitness.add((CommunityFitnessCalculator(3), 1))
# fitness.add((ModularityFitnessCalculator(0.7), 1))
#fitness.add((mean_degree_fit, 1))

#fitness.add((modularity_fit, 1))

#fitness.add((sir_days_fit, 1))
#fitness.add((eff_fit, 1))
#fitness.add((degree_sequence_fit, 1))
#fitness.add((motif_fit, 1))
#fitness.add((pl_fit, 1))


def run_program():
    save_directory_name = "test"
    #os.mkdir(save_directory_name)
    n = 1000
    #g = utilities.generate_initial_g(fitness, number_of_nodes=n)
    #g = igraph.Graph(500)
    #g = igraph.Graph.Erdos_Renyi(n=500, m=3000)
    #g = get_bankGraph('new_md')
    g = get_bankGraph('test_10')
    k = int(math.ceil(math.log(g.vcount(), 10)))
    print 'k: ', k
    k = 3 # 100
    system = NetworkModifier(g, fitness, edge_fixes=k, edge_removals=k, edge_rewirings=k)

    termometer_config = {'high_temperature': 50,
                         'low_temperature': 0.1,
                         'cooling_rate': 0.999,
                         'keep_temperature': 3}
    termometer = TemperatureKeeper(**termometer_config)

    algorithm = SimulatedAnnealing(termometer, system)
    print "alg type", type(algorithm)
    try:
        g, fit = algorithm.run_annealing()
    except KeyboardInterrupt:
        pass
    finally:
        print algorithm.system.best_proposal[1], fitness.print_fitness(algorithm.system.best_proposal[0])
        print "summary" , algorithm.system.best_proposal[0].summary()
        exporter = utilities.Exporter(save_directory_name)
        #exporter.visualize_correlation(algorithm)
        #exporter.visualize_actions(algorithm)
        #exporter.visualize_actions_smooth(algorithm)
        #form_edge_iter_viz(form_addE_arr(algorithm.edge_arr), type="acc")
        #igraphplus.metrics.degree_distribution_plot(algorithm.system.best_proposal[0])
        #print igraph.power_law_fit(algorithm.system.best_proposal[0].degree(), return_alpha_only=False)
        #exporter.visualize_actions_success(algorithm)
        #exporter.visualize_fitnesses(algorithm)
        print algorithm.system.fitness.print_fitness(algorithm.system.best_proposal[0])
        #open('EDGE_ARR.txt', 'w').write('\n'.join('%s %s' % x for x in algorithm.edge_arr))
        #open('PROP_ARR.txt', 'w').write('\n'.join('%s %s' % x for x in algorithm.prop_log))
        #open('ENERGY_ARR.txt', 'w').write('\n'.join('%s %s' % x for x in algorithm.energy_arr))
        #open('CLUST_ARR.txt', 'w').write('\n'.join('%s %s' % x for x in algorithm.clust_arr))
        #open('DENS_ARR.txt', 'w').write('\n'.join('%s %s' % x for x in algorithm.density_arr))
        print 'Fitness calls N: ', algorithm.system.fitness.calls
        print "CONVERTING TO GML"
        g.write("full_md.graphml", format='graphml')
        print "End converting".upper()
        print g.is_weighted()
        print g.ecount()


if __name__ == '__main__':
    run_program()
